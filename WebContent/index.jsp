
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tic-Tac-Toe</title>
<link href="css/mainstyle.css" rel="stylesheet" type="text/css" />
   <c:choose> 
	    <c:when test="${winner == 1}" > 
    	<script type='text/javascript'> alert('You won!');</script>
  	  </c:when> 
  	    <c:when test="${winner == 2}" > 
    	<script type='text/javascript'> alert('You lost!');</script>
  	  </c:when> 
	</c:choose>

</head>
<body>
  <div id="container">
   <div id="header">Tic-tac-toe</div>
		<div id="sidebar">
			<c:choose>
				<c:when test="${userName == null}">
					<p>
						<a href="login.jsp">sign in</a>
					</p>
				</c:when>
				<c:otherwise>
					<p><h2>Hello, ${userName}</h2>
					<p>
					<table>
						<tr>
							<td>wins</td>
							<td>${winsNum}</td>
						</tr>
						<tr>
							<td>losses</td>
							<td>${lossesNum}</td>
						</tr>						
					</table>
				</c:otherwise>
			</c:choose>
			<p>
				<a href="newgame">new game</a>
			</p>
		</div>


		<div id="content">
    <h2>YOU VS AI</h2>
   
      <c:choose> 
	    <c:when test="${game == null}" > 
    	<p>internal error (game is null)</p>
  	  </c:when> 
  	<c:otherwise> 
  	
  	<c:set var="maxrow" value="${game.getMAXROW()}" />
  	<c:set var="maxcol" value="${game.getMAXCOL()}" />
  	
	 <table border="1">
    <c:forEach var="i" begin="0" end="9">
	  <tr>
	  	<c:forEach var="j" begin="0" end="9">
	  	<td>		
	  	 	 <c:choose>
    			<c:when test="${game.getFigure(i,j)==2}"> 
    			   <img src="img/circle.png" alt="circle">
   				 </c:when>
   				 <c:when test="${game.getFigure(i,j)==1}">
    		  		<img src="img/cross.png" alt="cross">
   				 </c:when>
  				  <c:otherwise>			
  				  	  <c:if test="${winner==null}" >  
  				  	    	<a href="index?row=${i}&col=${j}">
  				  	   </c:if>		
  				  		<img src="img/empty.png" alt="empty">
  				  	  <c:if test="${winner==null}" >  
  				  	    	</a>
  				  	  </c:if>	  	
  				  				      
  			 	  </c:otherwise>
			</c:choose> 	
	  	 
	  	 </td>
		</c:forEach>
	  </tr>
	</c:forEach>    
    </table> 
 
 	 </c:otherwise> 
 	 </c:choose>  
	
   </div>
   
   <div id="footer">&copy; ncedu</div>
  </div>
  

  
 </body>
</html>

  