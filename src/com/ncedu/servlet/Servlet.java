package com.ncedu.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Servlet extends HttpServlet {

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String welcomeMessage = "Hello, " + name + "!";

		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<h1>" + welcomeMessage + "</h1>");
		out.println("<a href=/servletexample/pages/form.html>"
				+ "Click here to go back to input page " + "</a>");
		out.close();
	}

	// http://stackoverflow.com/questions/2349633/servlets-doget-and-dopost
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		session.setAttribute("userName", "Alex");
		response.sendRedirect("index.jsp");
		/*response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("Hello World start 43");
		DBInterface testDB=new GameDB();
		if (testDB==null){
			out.println("testDB is null. return");
			return;
		}
		UserInterface testUser=testDB.addUser("alextest", "alextest1", "password1");
		if (testUser==null){
			out.println("testUser is null. return");
			return;
		}
		out.println("Hello World 2 us");*/
	}

	public void destroy() {

	}
}
