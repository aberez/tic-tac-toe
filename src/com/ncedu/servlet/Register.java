package com.ncedu.servlet;

import javax.servlet.*;
import javax.servlet.http.*;

import com.ncedu.database.DBInterface;
import com.ncedu.database.GameDB;
import com.ncedu.database.GameUser;
import com.ncedu.database.UserInterface;

import java.io.*;
import java.sql.*;

public class Register extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);

		String name = request.getParameter("name");
		String login = request.getParameter("login");
		String password = request.getParameter("password");

		DBInterface gamedb = (DBInterface) session.getAttribute("db");
		if (gamedb == null) {
			out.println("DBInterface in register is null");
			try {
				gamedb = new GameDB();
			} catch (SQLException e) {
				e.printStackTrace(out);
				return;
			}
			session.setAttribute("db", gamedb);
		}

		if (gamedb.findLogin(login) == false) {/*������ ����� ��� �� �����*/
			UserInterface gameuser = gamedb.addUser(name, login, password);
			session.setAttribute("user", gameuser);
			session.setAttribute("userName",gameuser.getName());
			session.setAttribute("winsNum",gameuser.getWins());
			session.setAttribute("lossesNum",gameuser.getLoses());
			session.setAttribute("drawsNum",gameuser.getDraws());
			
			response.sendRedirect("index.jsp");
		} else {/*����� ��� �����*/
			RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
			rd.include(request, response);
			out.println("<script type='text/javascript'>	alert('this login is already taken!');	</script>");
		}

	}
}
