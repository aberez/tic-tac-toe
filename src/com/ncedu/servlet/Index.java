package com.ncedu.servlet;

import com.ncedu.database.UserInterface;
import com.ncedu.ncgame.*;

import java.io.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Index extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);
		Game game = (Game) session.getAttribute("game");
		if (game == null) {
			game = new Game();
			session.setAttribute("game", game);
			session.setAttribute("winner", null);
		}
		int row = 0;
		int col = 0;
		int flag = 0;
		String srow = request.getParameter("row");
		String scol = request.getParameter("col");
		if (srow != null) {
			row = Integer.parseInt(srow);
			flag = 1;
		}
		if (scol != null) {
			col = Integer.parseInt(scol);
			flag += 1;
		}
		if (flag == 2) {
			Position pos = new Position(row, col);
			game.makeMove(pos, 1);
			if (game.getWinner() == true) {
				//game = null;
				//session.setAttribute("game", game);
				int winner=1;
				session.setAttribute("winner", winner);
				
				
				
				UserInterface gameuser = (UserInterface) session.getAttribute("user");
				if (gameuser!=null){
					gameuser.addWins();
				}
				//out.println("<script type='text/javascript'> alert('You win!');</script>");
				//RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
				//rd.include(request, response);

				//out.close();
			}
			//game.makeRandomMove();
			game.makeMoveAI();
			if (game.getWinner() == true) {
				//game = null;
				//session.setAttribute("game", game);
				int winner=2;
				session.setAttribute("winner", winner);
				
				UserInterface gameuser = (UserInterface) session.getAttribute("user");
				if (gameuser!=null){
					gameuser.addLoses();
				}
				//RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
				//rd.include(request, response);
				//out.println("<script type='text/javascript'> alert('You lose!'); </script>");
				//out.close();
			}
		}
		UserInterface gameuser = (UserInterface) session.getAttribute("user");
		if (gameuser!=null){
			 session.setAttribute("winsNum",gameuser.getWins());
			 session.setAttribute("lossesNum",gameuser.getLoses());
			 session.setAttribute("drawsNum",gameuser.getDraws());
		}
		response.sendRedirect("index.jsp");
	}

}
