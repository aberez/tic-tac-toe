package com.ncedu.servlet;

import java.io.*;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ncedu.database.DBInterface;
import com.ncedu.database.GameDB;
import com.ncedu.database.GameUser;
import com.ncedu.database.UserInterface;

public class Login extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		HttpSession session = request.getSession(true);
		
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		DBInterface gamedb = (DBInterface) session.getAttribute("db");
		if (gamedb == null) {
			try {
				gamedb = new GameDB();
			} catch (SQLException e) {
				e.printStackTrace(out);
				return;
			}
			session.setAttribute("db", gamedb);
		}
		
		UserInterface gameuser = gamedb.loadUser(login, password);
		
		if (gameuser == null) {
			response.sendRedirect("login.jsp");
		} else {
			out.println("gameuser "+login + password+  " is NOT null");
			session.setAttribute("user", gameuser);
			session.setAttribute("userName",gameuser.getName());
	     	session.setAttribute("winsNum",gameuser.getWins());
		    session.setAttribute("lossesNum",gameuser.getLoses());
			session.setAttribute("drawsNum",gameuser.getDraws());
			
			response.sendRedirect("index.jsp");
		}
	}

}
