package com.ncedu.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLConnection {

	private String url = "jdbc:mysql://localhost:3306/ncgame";
	private String username = "java";
	private String password = "qwerty";

	
      	private Connection conn=null;
 
     
		static final SQLConnection connINSTANCE =  new SQLConnection();
      
		public static SQLConnection getInstance()  throws SQLException {
		     return connINSTANCE;
		  }
		
		public Connection getConnection(){
			return conn;
		}

		private SQLConnection(){
			try {
			   Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection(url, username, password);
			} catch (SQLException e) {
			    throw new RuntimeException("Cannot connect the database!", e);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Cannot open jdbc Driver!", e);
			}
	   }
		
		public void close() throws SQLException{
		        if(conn!= null)
		          conn.close();		     
		}


}
