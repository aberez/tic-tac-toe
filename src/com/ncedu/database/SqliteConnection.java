package com.ncedu.database;

/**
 * Singleton class that provides DB connection (sqlite in this particular case)  
 */



/**
 * @author Alex Berezin
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqliteConnection { 
		private Connection conn;
		private static final SqliteConnection connINSTANCE =  new SqliteConnection();
		
		
		public static SqliteConnection getInstance() {
		     return connINSTANCE;
		  }
		
		public Connection getConnection(){
			return conn;
		}

		private SqliteConnection() {
			try{
				String sDriverName = "org.sqlite.JDBC";
				Class.forName(sDriverName);
				String sDbUrl = "jdbc:sqlite:WEB-INF/ncgame.sqlite";
				conn = DriverManager.getConnection(sDbUrl);
			} 
			catch (ClassNotFoundException e){
				e.printStackTrace();
			}
			catch (SQLException e){
				e.printStackTrace();
			}
			

		}
		
		public void close(){
			 try
		      {
		        if(conn!= null)
		          conn.close();
		      }
		      catch(SQLException e)
		      {
		        e.printStackTrace();
		      }
		}
}

