package com.ncedu.database;

public interface UserInterface {
	String getName();
	
	void setName(String NAME);
	
	
	String getLogin();
	
	void setLogin(String LOGIN);
	
	void setPassword(String password);
	
	String getPassword();
	
	int getId();
	
	int getWins();
	
	void addWins();
	
	int getLoses();
	
	void addLoses();
	
	int getDraws();
	
	void addDraws();	

}
