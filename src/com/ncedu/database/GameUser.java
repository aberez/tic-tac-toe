/**
 * 
 */
package com.ncedu.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * @author Alex
 *
 */
public class GameUser implements UserInterface {
	private int id;
	private Connection conn;
	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getName()
	 */
	public GameUser(int ID, Connection CONN){
		id=ID;
		conn=CONN;
	}
	@Override
	public String getName(){
		ResultSet rs = null;
		Statement  searchName=null;		
		try {
			searchName=conn.createStatement();
			String query="SELECT name FROM userinfo WHERE userinfo.id="+id;
			rs=searchName.executeQuery(query);
			if (!rs.next()){
				return null;				
			}
			return rs.getString("name");
					
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchName!=null) searchName.close();
			}
			catch(Exception ignore){}
		}
		return null;
	}
	

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#setName(java.lang.String)
	 */
	@Override
	public void setName(String NAME) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getLogin()
	 */
	@Override
	public String getLogin() {
		ResultSet rs = null;
		Statement  searchLogin=null;		
		try {
			searchLogin=conn.createStatement();
			String query="SELECT login FROM userinfo WHERE userinfo.id="+id;
			rs=searchLogin.executeQuery(query);
			if (!rs.next()){
				return null;				
			}
			return rs.getString("login");
					
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchLogin!=null) searchLogin.close();
			}
			catch(Exception ignore){}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#setLogin(java.lang.String)
	 */
	@Override
	public void setLogin(String LOGIN) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getPassword()
	 */
	@Override
	public String getPassword() {
		ResultSet rs = null;
		Statement  searchPassword=null;		
		try {
			searchPassword=conn.createStatement();
			String query="SELECT password FROM userinfo WHERE userinfo.id="+id;
			rs=searchPassword.executeQuery(query);
			if (!rs.next()){
				return null;				
			}
			return rs.getString("password");
					
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchPassword!=null) searchPassword.close();
			}
			catch(Exception ignore){}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getId()
	 */
	@Override
	public int getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getWins()
	 */
	@Override
	public int getWins() {
		ResultSet rs = null;
		Statement  searchWins=null;		
		try {
			searchWins=conn.createStatement();
			String query="SELECT wins FROM userstat WHERE userstat.id="+id;
			rs=searchWins.executeQuery(query);
			if (!rs.next()){
				return 0;				
			}
			return rs.getInt("wins");
					
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchWins!=null) searchWins.close();
			}
			catch(Exception ignore){}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#addWins()
	 */
	@Override
	public void addWins() {
		int newWins=getWins()+1;
		Statement  updateWins=null;	
		try {
			updateWins=conn.createStatement();
			String query="UPDATE userstat SET wins="+newWins+" WHERE userstat.id="+id;
			updateWins.executeUpdate(query);
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (updateWins!=null) updateWins.close();
			}
			catch(Exception ignore){}
		}
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getLoses()
	 */
	@Override
	public int getLoses() {
		ResultSet rs = null;
		Statement  searchLoses=null;		
		try {
			searchLoses=conn.createStatement();
			String query="SELECT losses FROM userstat WHERE userstat.id="+id;
			rs=searchLoses.executeQuery(query);
			if (!rs.next()){
				return 0;				
			}
			return rs.getInt("losses");
					
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchLoses!=null) searchLoses.close();
			}
			catch(Exception ignore){}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#addLoses()
	 */
	@Override
	public void addLoses() {
		int newLoses=getLoses()+1;
		Statement  updateLoses=null;	
		try {
			updateLoses=conn.createStatement();
			String query="UPDATE userstat SET losses="+newLoses+" WHERE userstat.id="+id;
			updateLoses.executeUpdate(query);
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (updateLoses!=null) updateLoses.close();
			}
			catch(Exception ignore){}
		}

	}

	

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#getDraws()
	 */
	@Override
	public int getDraws() {
		ResultSet rs = null;
		Statement  searchDraws=null;		
		try {
			searchDraws=conn.createStatement();
			String query="SELECT draws FROM userstat WHERE userstat.id="+id;
			rs=searchDraws.executeQuery(query);
			if (!rs.next()){
				return 0;				
			}
			return rs.getInt("draws");
					
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchDraws!=null) searchDraws.close();
			}
			catch(Exception ignore){}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.ncedu.servlet.UserInterface#addDraws()
	 */
	@Override
	public void addDraws() {
		int newdraws=getDraws()+1;
		Statement  updateDraws=null;	
		try {
			updateDraws=conn.createStatement();
			String query="UPDATE userstat SET draws="+newdraws+" WHERE userstat.id="+id;
			updateDraws.executeUpdate(query);
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (updateDraws!=null) updateDraws.close();
			}
			catch(Exception ignore){}
		}

	}

}
