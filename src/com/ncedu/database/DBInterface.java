/**
 * 
 */
package com.ncedu.database;


/**
 * @author Alex
 *
 */
public interface DBInterface {

	UserInterface addUser(String name, String login, String password);
	
	void deleteUser (int id);
	
	UserInterface loadUser(String login, String password);
	
	/*checks if login is already taken*/
	boolean findLogin(String login);
	
		
}
