package com.ncedu.database;

import java.io.IOException;
import java.io.PrintWriter;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class SQLConnectionTest
 */
@WebServlet(
		description = "Test connection to mySQL NCGAME DB", 
		urlPatterns = { "/SQLConnectionTest" } 
		)

public class SQLConnectionTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SQLConnectionTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    //see http://stackoverflow.com/questions/2839321/java-connectivity-with-mysql for JDBC information about))
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			
		    out.println("Loading driver...");
		    Class.forName("com.mysql.jdbc.Driver");
		    out.println("Driver loaded!");
		} catch (ClassNotFoundException e) {
		    throw new ServletException("Cannot find the driver in the classpath!", e);
		}
		
		String url = "jdbc:mysql://localhost:3306/ncgame";
		String username = "java";
		String password = "qwerty";
		Connection connection = null;
		try {
		    out.println("Connecting database...");
		    connection = DriverManager.getConnection(url, username, password);
		   out.println("Database connected!");
		} catch (SQLException e) {
		    throw new RuntimeException("Cannot connect the database!", e);
		} finally {
		   out.println("Closing the connection.");
		    if (connection != null) try { connection.close(); } catch (SQLException ignore) {}
		}
	}

}
