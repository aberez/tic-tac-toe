package com.ncedu.database;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.*;




public class JUnittestDB {
	
	DBInterface testDB;
	@Before 
	public void createDBInterface() {
		
		try {
			testDB=new GameDB();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    }
	

	@Test
	@Ignore
	public void testAddUser() {
		UserInterface testUser=testDB.addUser("alex", "alex1", "password");
		assertNotNull(testUser);
	}

	@Test
	@Ignore
 	public void testLoadUser() {
		UserInterface testUser=testDB.loadUser("alex1", "password");
		String name = testUser.getName();
		assertEquals(name,"alex");
	}

	
	@Test
	@Ignore
	public void testFindLogin() {
		
		assertTrue (testDB.findLogin("beral"));
	}
	
	@Test
 	public void testAddWins() {
		UserInterface testUser=testDB.loadUser("alex1", "password");
		int wins=testUser.getWins();
		testUser.addWins();
		int newwins=testUser.getWins();
		assertEquals(newwins,wins+1);
	}
	@Test
 	public void testAddLoses() {
		UserInterface testUser=testDB.loadUser("alex1", "password");
		int losses=testUser.getLoses();
		testUser.addLoses();
		int newlosses=testUser.getLoses();
		assertEquals(newlosses,losses+1);
	}
	
	@Test
 	public void testAddDraws() {
		UserInterface testUser=testDB.loadUser("alex1", "password");
		int draws=testUser.getDraws();
		testUser.addDraws();
		int newdrawss=testUser.getDraws();
		assertEquals(newdrawss,draws+1);
	}


}
