/**
 * 
 */
package com.ncedu.database;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;






/**
 * @author Alex
 *
 */
public class GameDB implements DBInterface {

	private Connection conn;
	public static final String GET_MAX_ID = "SELECT MAX(id) FROM userinfo";
	public static final String INSERT_USER_INFO = "INSERT INTO userinfo (id,name,login,password) VALUES (?,?,?,?)";
	public static final String LOAD_USER = "SELECT * FROM userinfo WHERE userinfo.login=? and userinfo.password=?";
	public static final String DELETE_USER_INFO = "DELETE FROM userinfo WHERE userinfo.id=?";
	public static final String INSERT_USER_STATS = "INSERT INTO userstat (id,wins,losses,draws) VALUES (?,0,0,0)";
	public static final String DELETE_USER_STATS = "DELETE FROM userstat WHERE userstat.id=?";
	
	
	private PreparedStatement insertUserInfo;
	private PreparedStatement deleteUserInfo;
	private PreparedStatement loadUserInfo;
	private PreparedStatement insertUserStats;
	private PreparedStatement deleteUserStats;
	private PreparedStatement getMaxId;

	final int timeout=5;
	
	public GameDB() throws SQLException{
		conn=SQLConnection.getInstance().getConnection();	
	}
	@Override
	public UserInterface addUser(String name, String login, String password) {
		int userId;
		try {
			login=login.toLowerCase();
			if (findLogin(login)){
				//TODO add exception
			return null;
		}
			
			insertUserInfo = conn.prepareStatement(INSERT_USER_INFO);
			insertUserInfo.setQueryTimeout(timeout);	
			
			insertUserStats = conn.prepareStatement(INSERT_USER_STATS);
			insertUserStats.setQueryTimeout(timeout);
			
			getMaxId=conn.prepareStatement(GET_MAX_ID);
			getMaxId.setQueryTimeout(timeout);
			ResultSet maxId = getMaxId.executeQuery();
			maxId.next();
			userId= maxId.getInt(1)+1;
			getMaxId.close();
			
			insertUserInfo.setInt(1, userId);
			insertUserInfo.setString(2, name);
			insertUserInfo.setString(3, login);
			insertUserInfo.setString(4, password);
			insertUserInfo.executeUpdate();
			insertUserInfo.close();
			
			
			insertUserStats.setInt(1, userId);
			insertUserStats.executeUpdate();
			insertUserStats.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally{
			try{
				if (insertUserInfo!=null) insertUserInfo.close();
				if (insertUserStats!=null) insertUserStats.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return new GameUser(userId,conn);
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		try {
			deleteUserInfo = conn.prepareStatement(DELETE_USER_INFO);
			deleteUserInfo.setQueryTimeout(timeout);
			deleteUserInfo.setInt(1, id);
			deleteUserInfo.executeUpdate();
			
			deleteUserStats = conn.prepareStatement(DELETE_USER_STATS);
			deleteUserStats.setQueryTimeout(timeout);
			deleteUserStats.setInt(1, id);
			deleteUserStats.executeUpdate();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if (deleteUserInfo!=null) deleteUserInfo.close();
				if (deleteUserStats!=null) deleteUserStats.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public UserInterface loadUser(String login, String password) {
		ResultSet rs = null;
		try {
			loadUserInfo = conn.prepareStatement(LOAD_USER);
			loadUserInfo.setQueryTimeout(timeout);

			loadUserInfo.setString(1, login);
			loadUserInfo.setString(2, password);
			rs = loadUserInfo.executeQuery();
			
			if (!rs.next()){
				loadUserInfo.close();
				loadUserInfo.clearParameters();
				return null; // TODO send exception
				
				
			}
			int userId=rs.getInt("id");	
			
			return new GameUser(userId,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (loadUserInfo!=null) loadUserInfo.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean findLogin(String login){
		ResultSet rs = null;
		Statement  searchLogin=null;		
		try {
			searchLogin=conn.createStatement();
			String query="SELECT * FROM userinfo WHERE userinfo.login='"+login+"'";
			rs=searchLogin.executeQuery(query);
			if (!rs.next()){
				rs.close();
				searchLogin.close();
				return false;
			}
			else{
				rs.close();
				searchLogin.close();
				return true;
			}
				
		}
		catch(SQLException e){
			e.printStackTrace();			
		}
		finally{
			try{
				if (rs!=null) rs.close();
				if (searchLogin!=null) searchLogin.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return false;
	}


}
