package com.ncedu.ncgame;

import java.util.Scanner;

public class Game {
	
	public final int MAXROW = 10;
	public final int MAXCOL = 10;
	private Field field = new Field(MAXROW, MAXCOL);
	private Player firstPlayer = new Player(1);
	private AIPlayer secondPlayer = new AIPlayer(2, field);
	private boolean winner = false;
	private final int WINNUM = 5;
	private int step;
	
	public Field getField() {
		return field;
	}
	
	public int getMAXROW(){
		return (this.MAXROW);
	}
	public int getMAXCOL(){
		return (this.MAXCOL);
	}
	
	public boolean getWinner() {
		return winner;
	}
	
	private boolean checkWinner(Position pos, int figure) {
		int row = pos.getRow();
		int col = pos.getCol();
		int row_begin, row_end, col_begin, col_end;
		int diag_up, diag_down, diag_left, diag_right;
		int step = WINNUM - 1;
		int score;
		
		row_begin = (row - step > 0) ? (row - step) : 0;
		row_end = (row + step < MAXROW) ? (row + step) : (MAXROW - 1);
		col_begin = (col - step > 0) ? (col - step) : 0;
		col_end = (col + step < MAXCOL) ? (col + step) : (MAXCOL - 1);
		
		diag_up = (row - row_begin < col - col_begin) ? (row_begin) : (row - col + col_begin);
		diag_down = (row_end - row < col_end - col) ? (row_end) : (row + col_end - col);
		diag_right = (row - row_begin < col_end - col) ? (row_begin) : (row - col_end + col);
		diag_left = (row_end - row < col - col_begin) ? (row_end) : (row + col - col_end);
		
		// check on horizontal
		score = 0;
		for (int i = col_begin; i <= col_end; ++i) {
			if (field.getCell(new Position(row, i)).getFigure() == figure) {
				++score;
			} else {
				score = 0;
			}
			if (score >= WINNUM) {
				winner = true;
				return true;
			}
		}
		
		// check on vertical
		score = 0;
		for (int i = row_begin; i <= row_end; ++i) {
			if (field.getCell(new Position(i, col)).getFigure() == figure) {
				++score;
			} else {
				score = 0;
			}
			if (score >= WINNUM) {
				winner = true;
				return true;
			}
		}
		
		// check on main diagonal
		/*score = 0;
		for (int i = 0; i < diag_down - diag_up; ++i) {
			if (field.getCell(new Position(diag_up + i, diag_left + i)).getFigure() == figure) {
				++score;
			} else {
				score = 0;
			}
			if (score >= WINNUM) {
				winner = true;
				return true;
			}
		}*/
		score = 0;
		for (int i = -WINNUM + 1; i < WINNUM - 1; ++i) {
			try {
				if (field.getCell(new Position(row + i, col + i)).getFigure() == figure) {
					++score;
				} else {
					score = 0;
				}
				if (score >= WINNUM) {
					winner = true;
					return true;
				}
			} catch(Exception e) {
				score = 0;
			}
		}
		
		// check on secondary diagonal
		/*score = 0;
		for (int i = 0; i < diag_right - diag_left; ++i) {
			if (field.getCell(new Position(diag_up - i, diag_right + i)).getFigure() == figure) {
				++score;
			} else {
				score = 0;
			}
			if (score >= WINNUM) {
				winner = true;
				return true;
			}
		}*/
		score = 0;
		for (int i = -WINNUM + 1; i < WINNUM - 1; ++i) {
			try {
				if (field.getCell(new Position(row + i, col - i)).getFigure() == figure) {
					++score;
				} else {
					score = 0;
				}
				if (score >= WINNUM) {
					winner = true;
					return true;
				}
			} catch(Exception e) {
				score = 0;
			}
		}
		return false;
	}
	
	public void makeMove(Position pos, int figure) throws IllegalArgumentException {
		if (field.getCell(pos).isEmpty() == false) {
			throw new IllegalArgumentException("Cell is not empty.");
		}
		field.putFigure(pos, figure);
		field.setLastpos(pos);
		winner = checkWinner(pos, figure);
	}
	
	public void makeMoveAI() {
		int figure = 2;
		Position pos = secondPlayer.moveFigure();
		field.putFigure(pos, figure);
		field.setLastpos(pos);
		winner = checkWinner(pos, figure);
	}
	
	public void makeRandomMove() {
		int figure = 2;
		Position pos = secondPlayer.moveFigureStupidly();
		field.putFigure(pos, figure);
		field.setLastpos(pos);
		winner = checkWinner(pos, figure);
	}
	
	public int getFigure(int row,int col){
		int res;
		try{
			res = field.getCell(new Position(row,col)).getFigure();
		}
		catch (NullPointerException e){
			return 0;
		}
		return res; 
		 
	}
	
	public void start() {
		Position pos;
		int figure;
		int row, col;
		Scanner sc = new Scanner(System.in);
		step = 0;
		while (winner == false) {
			++step;
			for (int i = 0; i < MAXROW; ++i) {
				for (int j = 0; j < MAXCOL; ++j) {
					System.out.print(field.getCell(new Position(i, j)).getFigure());
				}
				System.out.print("\n");
			}
			System.out.print("\n");
			if (step % 2 == 1) {
				row = sc.nextInt();
				col = sc.nextInt();
				makeMove(new Position(row, col), 1);
				System.out.println("Your turn:" + row + " " + col);
			} else {
				makeMoveAI();
			}
			if (getWinner() == true) {
				if (step % 2 == 1) {
					System.out.println("Winner: you.");
				} else {
					System.out.println("Winner: AI.");
				}
				break;
			}
		}
		for (int i = 0; i < MAXROW; ++i) {
			for (int j = 0; j < MAXCOL; ++j) {
				System.out.print(field.getCell(new Position(i, j)).getFigure());
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}
	
}
