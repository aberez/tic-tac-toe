package com.ncedu.ncgame;

import java.util.ArrayList;
import java.util.Random;

public class AIPlayer {
	
	private final int MAXROW = 10;
	private final int MAXCOL = 10;
	private final int WINNUM = 5;
	private Field field;
	
	private final int figure;
	private Position pos = new Position(0, 0);
	private ArrayList<Position> figuresA;
	private ArrayList<Position> figuresB;
	
	Random randomGenerator = new Random();
	
	public AIPlayer(int figure, Field field) {
		this.figure = figure;
		this.field = field;
	}
	
	public int getFigure() {
		return figure;
	}
	
	Position moveFigureStupidly() {
		int row, col;
		while (true) {
			row = randomGenerator.nextInt(MAXROW);
			col = randomGenerator.nextInt(MAXCOL);
			pos.setRow(row);
			pos.setCol(col);
			if (field.isEmpty(pos) == true) {
				break;
			}
		}
		return pos;
	}
	
	double computeScorePostion(Field f, int a, int b) {
		int row = a;
		int col = b;
		int row_begin, row_end, col_begin, col_end;
		int step = WINNUM - 1;
		int score, score2;
		int score_best = 0, score_best2 = 0;
		
		row_begin = (row - step > 0) ? (row - step) : 0;
		row_end = (row + step < MAXROW) ? (row + step) : (MAXROW - 1);
		col_begin = (col - step > 0) ? (col - step) : 0;
		col_end = (col + step < MAXCOL) ? (col + step) : (MAXCOL - 1);
		
		// check on horizontal
		score = 0;
		score2 = 0;
		for (int i = col_begin; i < col; ++i) {
			if (f.getCell(new Position(row, i)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(row, i)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		score = 0;
		score2 = 0;
		for (int i = col_end; i > col; --i) {
			if (f.getCell(new Position(row, i)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(row, i)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		// check on vertical
		score = 0;
		score2 = 0;
		for (int i = row_begin; i < row; ++i) {
			if (f.getCell(new Position(i, col)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(i, col)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		score = 0;
		score2 = 0;
		for (int i = row_end; i > row; --i) {
			if (f.getCell(new Position(i, col)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(i, col)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		// check on main diagonal
		score = 0;
		score2 = 0;
		for (int i = -WINNUM + 1; i < 0; ++i) {
			try {
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		score = 0;
		score2 = 0;
		for (int i = WINNUM - 1; i > 0; --i) {
			try {
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		// check on secondary diagonal
		score = 0;
		score2 = 0;
		for (int i = -WINNUM + 1; i < 0; ++i) {
			try {
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		score = 0;
		score2 = 0;
		for (int i = WINNUM - 1; i > 0; --i) {
			try {
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += Math.exp(score + 1);
		score_best2 += Math.exp(score2 + 1);
		
		score_best *= 3;
		score_best2 *= 3;
		
		double q = 0.5;
		return score_best2 + q * score_best;
	}
	
	Position computeScore() {
		Position res = new Position(0, 0);
		double best = 0, score = 0;
		for (int i = 0; i < MAXROW; ++i) {
			for (int j = 0; j < MAXCOL; ++j) {
				score = computeScorePostion(field, i, j);
				if (score >= best && field.isEmpty(new Position(i, j))) {
					best = score;
					res.setRow(i);
					res.setCol(j);
				}
			}
		}
		return res;
	}
	
	Position moveFigure() {
		int alpha = -1000;
		int beta = 1000;
		int depth = 2;
		int player = 2;
		Field f = new Field(field);
		FieldContainer fc = new FieldContainer(f);
		//alphabeta(alpha, beta, depth, player, fc);
		//return fc.getField().getLastpos();
		return computeScore();
	}
	
	void gen_moves(ArrayList<Field> moves, Field f, int player) {
		int a = f.getLastpos().getRow();
		int b = f.getLastpos().getCol();
		for (int i = -10; i <= 10; ++i) {
			for (int j = -10; j <= 10; ++j) {
				try {
					Position newpos = new Position(a + i, b + j);
					if (f.isEmpty(newpos) == true) {
						Field newf = new Field(f);
						newf.putFigure(newpos, player);
						newf.setLastpos(newpos);
						moves.add(newf);
					}
				} catch (Exception e) {
					
				}
			}
		}
	}
	
	int estimate(Field f, int player) { //0..255
		int row = f.getLastpos().getRow();
		int col = f.getLastpos().getCol();
		int row_begin, row_end, col_begin, col_end;
		int step = WINNUM - 1;
		int score, score2;
		int score_best = 0, score_best2 = 0;
		
		row_begin = (row - step > 0) ? (row - step) : 0;
		row_end = (row + step < MAXROW) ? (row + step) : (MAXROW - 1);
		col_begin = (col - step > 0) ? (col - step) : 0;
		col_end = (col + step < MAXCOL) ? (col + step) : (MAXCOL - 1);
		
		// check on horizontal
		score = 0;
		score2 = 0;
		for (int i = col_begin; i <= col_end; ++i) {
			if (f.getCell(new Position(row, i)).getFigure() == 1) {
				++score;
			} else {
				if (score > score_best) {
					score_best = score;
				}
				score = 0;
			}
			if (f.getCell(new Position(row, i)).getFigure() == 2) {
				++score2;
			} else {
				if (score2 > score_best2) {
					score_best2 = score2;
				}
				score2 = 0;
			}
		}
		
		// check on vertical
		score = 0;
		score2 = 0;
		for (int i = row_begin; i <= row_end; ++i) {
			if (f.getCell(new Position(i, col)).getFigure() == 1) {
				++score;
			} else {
				if (score > score_best) {
					score_best = score;
				}
				score = 0;
			}
			if (f.getCell(new Position(i, col)).getFigure() == 2) {
				++score2;
			} else {
				if (score2 > score_best2) {
					score_best2 = score2;
				}
				score2 = 0;
			}
		}
		
		// check on main diagonal
		score = 0;
		score2 = 0;
		for (int i = -WINNUM + 1; i < WINNUM - 1; ++i) {
			try {
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 1) {
					++score;
				} else {
					if (score > score_best) {
						score_best = score;
					}
					score = 0;
				}
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 2) {
					++score2;
				} else {
					if (score2 > score_best2) {
						score_best2 = score2;
					}
					score2 = 0;
				}
			} catch(Exception e) {
				if (score > score_best) {
					score_best = score;
				}
				score = 0;
				if (score2 > score_best2) {
					score_best2 = score2;
				}
				score2 = 0;
			}
		}
		
		// check on secondary diagonal
		score = 0;
		score2 = 0;
		for (int i = -WINNUM + 1; i < WINNUM - 1; ++i) {
			try {
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 1) {
					++score;
				} else {
					if (score > score_best) {
						score_best = score;
					}
					score = 0;
				}
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 2) {
					++score2;
				} else {
					if (score2 > score_best2) {
						score_best2 = score2;
					}
					score2 = 0;
				}
			} catch(Exception e) {
				if (score > score_best) {
					score_best = score;
				}
				score = 0;
				if (score2 > score_best2) {
					score_best2 = score2;
				}
				score2 = 0;
			}
		}
		
		if (score_best >= WINNUM) {
			return 0;
		}
		if (score_best2 >= WINNUM) {
			return 255;
		}
		if (player == 1) {
			return 127 - 2 * score_best;
		} else {
			return 127 + score_best2;
		}
	}
	
	int estimateComplex(Field f, int player) {
		int row = f.getLastpos().getRow();
		int col = f.getLastpos().getCol();
		int row_begin, row_end, col_begin, col_end;
		int step = WINNUM - 1;
		int score, score2;
		int score_best = 0, score_best2 = 0;
		
		row_begin = (row - step > 0) ? (row - step) : 0;
		row_end = (row + step < MAXROW) ? (row + step) : (MAXROW - 1);
		col_begin = (col - step > 0) ? (col - step) : 0;
		col_end = (col + step < MAXCOL) ? (col + step) : (MAXCOL - 1);
		
		// check on horizontal
		score = 0;
		score2 = 0;
		for (int i = col_begin; i < col; ++i) {
			if (f.getCell(new Position(row, i)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(row, i)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		score = 0;
		score2 = 0;
		for (int i = col_end; i > col; --i) {
			if (f.getCell(new Position(row, i)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(row, i)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		// check on vertical
		score = 0;
		score2 = 0;
		for (int i = row_begin; i < row; ++i) {
			if (f.getCell(new Position(i, col)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(i, col)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		score = 0;
		score2 = 0;
		for (int i = row_end; i > row; --i) {
			if (f.getCell(new Position(i, col)).getFigure() == 1) {
				++score;
			} else {
				score = 0;
			}
			if (f.getCell(new Position(i, col)).getFigure() == 2) {
				++score2;
			} else {
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		// check on main diagonal
		score = 0;
		score2 = 0;
		for (int i = -WINNUM + 1; i < 0; ++i) {
			try {
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		score = 0;
		score2 = 0;
		for (int i = WINNUM - 1; i > 0; --i) {
			try {
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col + i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		// check on secondary diagonal
		score = 0;
		score2 = 0;
		for (int i = -WINNUM + 1; i < 0; ++i) {
			try {
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		score = 0;
		score2 = 0;
		for (int i = WINNUM - 1; i > 0; --i) {
			try {
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 1) {
					++score;
				} else {
					score = 0;
				}
				if (f.getCell(new Position(row + i, col - i)).getFigure() == 2) {
					++score2;
				} else {
					score2 = 0;
				}
			} catch(Exception e) {
				score = 0;
				score2 = 0;
			}
		}
		score_best += score + 1;
		score_best2 += score2 + 1;
		
		score_best *= 3;
		score_best2 *= 3;
		
		double q = 0.1;
		if (player == 2) {
			return (int) (127 + (score_best2 + q * score_best));
		} else {
			return (int) (127 + (score_best + q * score_best2));
		}
	}
	
	int alphabeta(int alpha, int beta, int depth, int player, FieldContainer fc) {
	    if (depth == 0) {
	        return estimateComplex(fc.getField(), player);
	    }
	    int best, score;
	    ArrayList<Field> moves = new ArrayList<Field>();
	    gen_moves(moves, fc.getField(), player);
	    if (player == 2) {
	        best = alpha;
	        for (int i = 0; i < moves.size(); ++i) {
	            if (best > beta) {
	                break;
	            }
	            score = alphabeta(best, beta, depth - 1, 1, new FieldContainer(moves.get(i)));
	            if (score > best) {
	                fc.setField(moves.get(i));
	                best = score;
	            }
	        }
	        return best;
	    } else {
	        best = beta;
	        for (int i = 0; i < moves.size(); ++i) {
	            if (best < alpha) {
	                break;
	            }
	            score = alphabeta(alpha, best, depth - 1, 2, new FieldContainer(moves.get(i)));
	            if (score < best) {
	            	fc.setField(moves.get(i));
	                best = score;
	            }
	        }
	        return best;
	    }
	}
	
}
