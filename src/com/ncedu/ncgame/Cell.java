package com.ncedu.ncgame;

/**
 * This class is for cell in game field.
 * @author greexon
 *
 */
public class Cell {
	
	private final Position pos;
	private int figure;
	private boolean empty;
	
	/**
	 * 
	 * @param pos The position, where this cell is situated.
	 */
	public Cell(Position pos)
	{
		this.pos = pos;
		this.empty = true;
	}
	
	public Cell(Cell c) {
		this.pos = new Position(c.getPos().getRow(), c.getPos().getCol());
		this.figure = c.getFigure();
		this.empty = c.isEmpty();
	}
	
	/**
	 * 
	 * @return The position, where this cell is situated.
	 */
	public Position getPos() {
		return pos;
	}

	/**
	 * 
	 * @return Type of the figure on this cell: 1 - first player, 2 - second player, 0 - no figure.
	 */
	public int getFigure() {
		return figure;
	}

	/**
	 * 
	 * @param figure Figure is going to put in this cell.
	 * @throws IllegalArgumentException On argument error.
	 */
	public void setFigure(int figure) throws IllegalArgumentException {
		if (figure != 1 && figure != 2) {
			throw new IllegalArgumentException("Type of the figure is not allowed.");
		}
		empty = false;
		this.figure = figure;
	}
	
	/**
	 * 
	 * @return True if cell is empty, false otherwise.
	 */
	public boolean isEmpty() {
		return empty;
	}

}
