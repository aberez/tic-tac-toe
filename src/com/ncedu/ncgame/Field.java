package com.ncedu.ncgame;

/**
 * This class is for field in game.
 * @author greexon
 *
 */
public class Field {
	
	private int maxrow;
	private int maxcol;
	private final Cell[][] field;
	private Position lastpos;
	
	/**
	 * 
	 * @param maxrow Maximum number of rows.
	 * @param maxcol Maximum number of columns.
	 */
	public Field(int maxrow, int maxcol) {
		this.maxrow = maxrow;
		this.maxcol = maxcol;
		field  = new Cell[maxrow][maxcol];
		for (int i = 0; i < maxrow; ++i) {
			for (int j = 0; j < maxcol; ++j) {
				field[i][j] = new Cell(new Position(i, j));
			}
		}
	}
	
	public Field(Field f) {
		this.maxrow = f.maxrow;
		this.maxcol = f.maxcol;
		field  = new Cell[maxrow][maxcol];
		for (int i = 0; i < maxrow; ++i) {
			for (int j = 0; j < maxcol; ++j) {
				field[i][j] = new Cell(f.getCell(new Position(i, j)));
			}
		}
		this.lastpos = new Position(f.getLastpos().getRow(), f.getLastpos().getCol());
	}
	
	/**
	 * 
	 * @param pos Position is to be checked.
	 * @throws IllegalArgumentException On argument error.
	 */
	private void checkPos(Position pos) throws IllegalArgumentException {
		int row = pos.getRow();
		int col = pos.getCol();
		if (row < 0 || row >= maxrow || col < 0 || col >= maxcol) {
			throw new IllegalArgumentException("Row and column out of field." + row + " " + col);
		}
	}
	
	/**
	 * 
	 * @param pos Position of the cell.
	 * @return Cell with row and column.
	 */
	public Cell getCell(Position pos)
	{
		checkPos(pos);
		return field[pos.getRow()][pos.getCol()];
	}
	
	/**
	 * 
	 * @param row Row of the cell.
	 * @param col Column of the cell.
	 * @param figure Figure is to be put.
	 */
	public void putFigure(Position pos, int figure) {
		checkPos(pos);
		field[pos.getRow()][pos.getCol()].setFigure(figure);
	}
	
	/**
	 * 
	 * @param row Row is to be checked.
	 * @param col Column is to be checked.
	 * @return True if cell is empty, false otherwise.
	 */
	public boolean isEmpty(Position pos) {
		checkPos(pos);
		return field[pos.getRow()][pos.getCol()].isEmpty();
	}

	public Position getLastpos() {
		return lastpos;
	}

	public void setLastpos(Position lastpos) {
		this.lastpos = lastpos;
	}
}
