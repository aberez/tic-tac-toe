package com.ncedu.ncgame;

import java.util.Scanner;

public class Player {
	
	private final int figure;
	Scanner in = new Scanner(System.in);
	
	public Player(int figure) {
		this.figure = figure;
	}
	
	public int getFigure() {
		return figure;
	}
	
	Position moveFigure() {
		int row = in.nextInt();
		int col = in.nextInt();
		return new Position(row, col);
	}
	
}
