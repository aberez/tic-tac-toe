package com.ncedu.ncgame;

public class FieldContainer {
	private Field field;
	
	public FieldContainer(Field f) {
		this.field = f;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}
}
